package com.tc.itfarm.dao;

import com.tc.itfarm.api.model.SingleTableDao;
import com.tc.itfarm.model.Log;
import com.tc.itfarm.model.LogCriteria;

public interface LogDao extends SingleTableDao<Log, LogCriteria> {
}